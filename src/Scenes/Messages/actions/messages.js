import {
    MESSAGES_LOAD_REQUEST,
    MESSAGES_RESET_STATE
} from '../actionTypes';

export const messagesLoadItems = () => ({
    type: MESSAGES_LOAD_REQUEST,
});

export const resetStateMessages = () => ({
    type: MESSAGES_RESET_STATE,
});