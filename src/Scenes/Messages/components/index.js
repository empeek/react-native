import {
    View,
    Text,
    Image,
    ScrollView,
    ActivityIndicator,
    TouchableOpacity
} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {
    messagesStyles,
} from './styles';

import {
    messagesLoadItems,
    resetStateMessages
} from '../actions';


class Messages extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.messagesLoadItems();
    }

    componentWillUnmount() {
        this.props.resetStateMessages();
    }

    render() {
        const {routes} = this.context;
        // if (this.props.error) {
        //     alert(
        //         this.props.error
        //     );
        // }
        return (
            (this.props.isLoading) ?
                <View style={messagesStyles.spinner}>
                    <ActivityIndicator size="large"/>
                </View>
                :
                <View style={messagesStyles.container}>
                    <Text>Hello!!!</Text>
                </View>
        );
    }
}

Messages.contextTypes = {
    routes: React.PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const messages = state.get('messages');
    return {
        isLoading: messages.get('isLoading'),
        messages: messages.get('messagesList'),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        messagesLoadItems,
        resetStateMessages
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Messages);