import {Map, List, fromJS} from 'immutable';

const initialState = fromJS({
    error: null,
    isLoading: false,
    data: null,
});


const messages = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default messages;