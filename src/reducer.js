import Messages from './Scenes/Messages';


import {combineReducers} from 'redux-immutable';

const applicationReducers = {
    messages: Messages.reducers.messages
};

const rootReducer = combineReducers(applicationReducers);


export default rootReducer;
