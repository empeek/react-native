// import Messages from './Scenes/Messages/components';
import {Text,TextInput,Platform} from 'react-native';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import configureStore from './store';
import Settings from './settings';

const settings = Settings.load();

const store = configureStore();

function setup() {
    class Root extends Component {
        render() {
            return (
                <Provider store={store}>
                    <Text>Hello</Text>
                </Provider>
            );
        }
    }

    return Root;
}

module.exports = setup;
